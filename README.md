# **Container-ElasticSearch**

This is an implementation of Elastic Search using Docker Containers on Kubernetes (or other platform like DC/OS in the future).  This is similar to other ElasticSearch related projects, it explores technologies available on any given platform.

The goal is to research container orchestration with elasticsearch as an interesting use case for such platforms.

## **K8S (Kubernetes) Platforms**

 * AWS
   * EKS 
     * https://aws.amazon.com/eks/
     * https://aws.amazon.com/blogs/aws/amazon-elastic-container-service-for-kubernetes/
   * KOPS - https://kubernetes.io/docs/getting-started-guides/kops/
   * EC2 - https://kubernetes.io/docs/getting-started-guides/aws/
 * Google
   * GCE - https://kubernetes.io/docs/getting-started-guides/gce/
   * GKE - https://cloud.google.com/kubernetes-engine/
 * AKS - https://docs.microsoft.com/en-us/azure/aks/
 * DC/OS
   * https://kubernetes.io/docs/getting-started-guides/dcos/
   * https://github.com/mesosphere/dcos-kubernetes-quickstart
 * Develoment (Local)
   * MiniKube
     * https://kubernetes.io/docs/getting-started-guides/minikube/
     * https://kubernetes.io/docs/tasks/tools/install-minikube/
     * https://github.com/kubernetes/minikube
 * Hard Way
   * https://deis.com/blog/2016/kubernetes-the-hard-way/
   * https://github.com/kelseyhightower/kubernetes-the-hard-way

## **K8S (Kubernetes) Deployments**

 * Kubectl
   * https://kubernetes.io/docs/reference/kubectl/overview/
   * https://kubernetes.io/docs/tasks/tools/install-kubectl/
 * Helm (https://helm.sh/)
   * https://deis.com/blog/2015/why-kubernetes-needs-helm/
   * https://platform9.com/blog/kubernetes-helm-why-it-matters/
   * https://github.com/kubernetes/helm/blob/master/docs/install.md
   * https://docs.helm.sh/architecture/
   * https://deis.com/blog/2015/introducing-helm-for-kubernetes/
   * https://kubernetes.io/docs/tasks/service-catalog/install-service-catalog-using-helm/
   * https://cloudacademy.com/blog/deploying-kubernetes-applications-with-helm/
 * Terraform
   * https://www.terraform.io/docs/providers/kubernetes/index.html
   * https://www.terraform.io/docs/providers/kubernetes/guides/getting-started.html
 * Mixed
   * https://dzone.com/articles/terraform-vs-helm-for-kubernetes
   * http://blog.infracloud.io/terraform-helm-kubernetes/
   
## **ElasticSearch**

 * Docker
   * https://hub.docker.com/_/elasticsearch/
   * https://www.docker.elastic.co/
   * https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html
   * https://stefanprodan.com/2016/elasticsearch-cluster-with-docker/
   * https://github.com/dockerfile/elasticsearch
   * https://www.elastic.co/blog/how-to-make-a-dockerfile-for-elasticsearch
 * Kubernetes
   * https://github.com/pires/kubernetes-elasticsearch-cluster
   * kops - https://dzone.com/articles/deploy-elasticsearch-with-kubernetes-on-aws-in-10
   * https://solinea.com/blog/elasticsearch-meets-kubernetes-part-1-2
   * https://solinea.com/blog/elasticsearch-meets-kubernetes-part-2-2
   * https://jeffmendoza.github.io/kubernetes/v1.0/examples/elasticsearch/README.html
   * https://github.com/jeffmendoza/kubernetes.github.io
 * DC/OS
   * https://github.com/mesosphere/elasticsearch-mesos
   * https://github.com/mesos/elasticsearch
   * http://mesos-elasticsearch.readthedocs.io/en/latest/
   * https://docs.portworx.com/scheduler/mesosphere-dcos/elasticsearch.html
